#!/bin/bash

gpg --keyserver keyring.debian.org --recv-key DF9B9C49EAA9298432589D76DA87E80D6294BE9B 

URL="https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/"
FILES="SHA512SUMS SHA512SUMS.sign debian-10.9.0-amd64-netinst.iso"

for i in $FILES
do
echo $i
wget -c $URL/$i
done

sha512sum -c SHA512SUMS
gpg --verify SHA512SUMS.sign SHA512SUMS
