#!/bin/bash

gpg --keyid-format long --keyserver hkp://keyserver.ubuntu.com --recv-keys 0x46181433FBB75451 0xD94AA3F0EFE21092

URL="http://releases.ubuntu.mirrors.uk2.net/18.04.3/"
FILES="SHA256SUMS SHA256SUMS.gpg ubuntu-18.04.3-desktop-amd64.iso"

for i in $FILES
do
echo $i
wget -c $URL/$i
done

sha256sum -c SHA256SUMS
gpg --verify SHA256SUMS.gpg SHA256SUMS